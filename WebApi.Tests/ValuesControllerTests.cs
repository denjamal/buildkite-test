using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using WebApi.Controllers;

namespace WebApi.Tests
{
    [TestClass]
    public class ValuesControllerTests
    {
        [TestMethod]
        public void Get_ShouldReturnStringArray_Success()
        {
            var expectedResult = new List<string>() { "value1", "value2" };
            var controller = new ValuesController();

            var result = controller.Get().Value.ToList();

            CollectionAssert.AreEqual(expectedResult, result);
        }

        //[TestMethod]
        //public void ThisTestShouldFailBuild()
        //{
        //    Assert.IsTrue(true);
        //}

    }
}

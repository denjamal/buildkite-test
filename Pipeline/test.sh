#!/bin/bash

# Exit when any command fails
set -e

# Build the container
docker build -t unit-tests --no-cache -f ./Dockerfile .
# Run the container
docker run -t unit-tests
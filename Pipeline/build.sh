#!/bin/bash

# Exit when any command fails
set -e

docker build -t $BUILDKITE_COMMIT --no-cache -f ./build.Dockerfile . # Any environment vars you want exposed you should pass in like so: '--build-arg API_URL=$API_URL'

FROM microsoft/dotnet:2.1-sdk AS build-env
WORKDIR /app
 
# Copy csproj and restore as distinct layers
COPY CoreTest.sln ./
COPY . .
 
# restore for all projects
RUN dotnet restore CoreTest.sln

ENTRYPOINT ["dotnet", "test"]

